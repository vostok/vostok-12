About
-----
Vostok is a small set of customizations for the Android Open Source Project.
Unlike many other projects, it's just a set of patches on top of AOSP. Only
a few [own repositories](https://gitlab.com/groups/vostok) have been added.

Supported Devices
-----------------
| Name                    | Codename  | Branch            | Build              |
| ----------------------- | --------- | ----------------- | ------------------ |

Setting Up Development System
-----------------------------
Install prerequisites (assuming you run Debian 11 Bullseye):

    sudo apt-get update
    sudo apt-get install --yes curl gpg python3 python3-git libncurses5 zip unzip libfreetype6 fontconfig rsync libarchive-tools jq
    mkdir ~/bin
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    mkdir -p ~/.repoconfig/gnupg
    chmod 700 ~/.repoconfig/gnupg
    GNUPGHOME=~/.repoconfig/gnupg gpg --keyserver hkp://hkps.pool.sks-keyservers.net --recv-keys 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
    curl https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ~/bin/repo
    export PATH=$PATH:$HOME/bin
    chmod a+x ~/bin/repo

Configure Git:

    git config --global user.name "Your Name"
    git config --global user.email "you@example.com"

Enable [ccache](https://ccache.samba.org) to speed up subsequent builds:

    export CCACHE_EXEC=/usr/bin/ccache
    export USE_CCACHE=1
    ccache -M 6G

Building
--------
Select the branch and codename of your device from the table above.

Download the source code:

    mkdir android
    cd android
    repo init -u https://android.googlesource.com/platform/manifest -b {BRANCH} --partial-clone --clone-filter=blob:limit=10M
    mkdir -p .repo/local_manifests
    curl https://vostok.gitlab.io/vostok-12/vostok.xml > .repo/local_manifests/vostok.xml
    repo sync -c

You'll need a lot of disk space and some patience.

Change current directory to the Android source tree root and build:

    source build/envsetup.sh    # add all needed commands into PATH
    lunch aosp_{CODENAME}-user  # select the target device
    ./vendor/vostok/apply       # apply customizations
    m dist                      # build the OS (target files)

It will take some time to compile all the bits of the operating system. Output
images will be signed with test keys. Never flash those images! This is a huge
security risk because those keys are well-known.

You must generate your own set of release keys and keep them private (do this
only once):

    ./vendor/vostok/genkeys ../release-keys

Sign the image with your release keys:

    sign_target_files_apks -o -d ../release-keys/ out/dist/aosp_{CODENAME}-target_files-eng.robot.zip out/dist/{BUILD}-$(date +%m%d).target_files.zip
    img_from_target_files out/dist/{BUILD}-$(date +%m%d).target_files.zip out/dist/{BUILD}-$(date +%m%d).fastboot.zip

You'll find the output in `out/dist/{BUILD}-$(date +%m%d).fastboot.zip`. Time
to run it!

Running
-------
Unlock the bootloader of your device, boot it into the fastboot mode and flash
the images:

    fastboot update {BUILD}-$(date +%m%d).fastboot.zip

The device will reboot automatically. Welcome to your new system!
